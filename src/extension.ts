/**************************************************************************
 * EDU-CIAA-FPGA
 * 
 * This extension provides some utilities to debug, simulate and synthesize
 * VHDL or Verilog code for the EDU-CIAA-FPGA board (based on an iCE40 FPGA)
 * It requires some Docker images to run as backend.
 * 
 * Ramiro Adrian Ghignone
 * 2021
 * 
 * DO NOT EDIT THE AUTOMATICALLY GENERATED /out JS CODE !!!!
 * 
 **************************************************************************/

// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as cp from 'child_process';
import { resolve } from 'dns';
import { stdout } from 'process';
import * as fs from 'fs';

// ----------------------------------- COMMANDS DEFINITION ----------------------------------- //

var commandsList = {
    "commands" : [
        {
            "name" : "ghdl_docker",
            "args" : [
                "Testbench Entity"
            ],
            "bash" : "ghdl",
            "info" : "Running GHDL from Docker..."
        },
        {
            "name" : "vunit_docker",
            "args" : [
				"VUnit Main Python file (include .py extension)"
			],
            "bash" : "vunit",
            "info" : "Running VUnit from Docker..."
        },
        {
            "name" : "python",
            "args" : [
				"Python file (include .py extension)"
			],
            "bash" : "vunit",
            "info" : "Running Python from Docker..."
        },
        {
            "name" : "vhdl_toolchain_docker",
            "args" : [
				"PCF File (include .pcf extension)",
				"Top Level Entity name"
			],
            "bash" : "vhdl_toolchain",
            "info" : "Full VHDL Toolchain from Docker..."
        },
        {
            "name" : "vhdl_syn_docker",
            "args" : [
				"PCF File (include .pcf extension)",
				"Top Level Entity name"
			],
            "bash" : "vhdl_syn",
            "info" : "VHDL to bitstream from Docker..."
        },
        {
            "name" : "iverilog_docker",
            "args" : [
				"Output Waveform (include .vcd or .ghw extension)",
				"Testbench File (include .v extension)",
			],
            "bash" : "iverilog",
            "info" : "Running Icarus Verilog from Docker..."
        },
        {
            "name" : "verilog_toolchain_docker",
            "args" : [
				"PCF File (include .pcf extension)",
				"Top Level File (include .v extension)"
			],
            "bash" : "verilog_toolchain",
            "info" : "Full Verilog toolchain from Docker..."
        },
        {
            "name" : "verilog_syn_docker",
            "args" : [
				"PCF File (include .pcf extension)",
				"Top Level File (include .v extension)"
			],
            "bash" : "verilog_syn",
            "info" : "Verilog to bitstream from Docker..."
        },
        {
            "name" : "bash",
            "args" : [
				"Bash Script (include .sh extension)"
			],
            "bash" : "bash",
            "info" : "Running Bash Script from Docker..."
        },
        {
            "name" : "doxygen_init",
            "args" : [],
            "bash" : "doxygen_init",
            "info" : "Initializing Doxygen from Docker..."
        },
        {
            "name" : "doxygen_run",
            "args" : [],
            "bash" : "doxygen_run",
            "info" : "Running Doxygen from Docker..."
        }
    ]
};

//----------------------------------------------------------------------------------------------//

// This method is called when the extension is activated
// the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, extension "EDU-CIAA-FPGA" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json

	// ================================= COMMANDS DEFINITION ================================= //

	// Commands are automatically generated from the JSON variable in 'commands.json'
	console.log(commandsList);
	commandsList.commands.forEach(element => {
		console.log("Registering: ".concat('educiaafpga.').concat(element.name));
		let disposable = vscode.commands.registerCommand('educiaafpga.'.concat(element.name), () => {

			// Display a message box to the user
			vscode.window.showInformationMessage(element.info);
	
			// Get current directory
			let folder = vscode.workspace.workspaceFolders?.[0].uri.path;
			if (typeof folder==='undefined') {
				vscode.window.showErrorMessage(`ERROR : Current directory is not a valid project folder`);
				return;
			}else{
				// Do something graceful
			}

			// Ask user for inputs using InputBoxes and callbacks... instead of Promises :-)
			let argsVector  : string[] = [folder];
			let inputVector : string[] = [];
			let inboxVec : vscode.InputBox[] = [];

			// Get script syntax for the current OS
			let scriptsFolder : string;
			let scriptsFormat : string;
			let scriptsRunCmd : string;
			let scriptsSepCmd : string;
			let scriptsSepSym : string;

			// Get paths
			let srcPath :string = __dirname;
			let cmd : string;
			let currentFolder : string;
			if(vscode.workspace.workspaceFolders!==undefined){
				currentFolder = vscode.workspace.workspaceFolders[0].uri.path;
			}else{
				console.error("Please open a folder project...");
				currentFolder = " ";
			}

			if(process.platform==="win32"){
				scriptsFolder = "windows";
				scriptsFormat = ".bat";
				scriptsRunCmd = ".\\";
				scriptsSepCmd = "&&";
				scriptsSepSym = "\\";
				currentFolder = currentFolder.split("/").slice(1).join(scriptsSepSym);
			}else if(process.platform==="linux"){
				scriptsFolder = "linux";
				scriptsFormat = ".sh";
				scriptsRunCmd = "./";
				scriptsSepCmd = "&&";
				scriptsSepSym = "/";
				currentFolder = "/"+currentFolder.split("/").slice(1).join(scriptsSepSym);
			}else{
				scriptsFolder = " ";
				scriptsFormat = " ";
				scriptsRunCmd = " ";
				scriptsSepCmd = " ";
				scriptsSepSym = " ";
			}

			// Build prelude of the target command
			cmd = `cd ${srcPath} ${scriptsSepCmd}`+
				   `cd .. ${scriptsSepCmd}`+
				   `cd scripts ${scriptsSepCmd}`+
				   `cd ${scriptsFolder} ${scriptsSepCmd}`+
				   `${scriptsRunCmd}${element.bash}${scriptsFormat}`+
				   ` ${currentFolder} `;//It's important to leave the blank spaces here
			console.log(cmd);
			console.log("---------------------------------------------------------");

			// Add arguments
			if(element.args.length>0){
				inboxVec = [];
				for (let index = 0; index < element.args.length; index++) {
					let newBox = vscode.window.createInputBox();
					newBox.prompt = element.args[index];
					newBox.onDidAccept((e)=>{
						inputVector.push(newBox.value);
						newBox.hide();
						if(index>0){
							inboxVec[index-1].show();
						}else{
							cmd+=inputVector.reverse().reduce((s1,s2)=>{return s2.concat(" ").concat(s1);}," ");
							console.log("Running :\n".concat(cmd));
							let command = cp.exec(cmd,(err,stdout,stderr)=>{

								if (err) {console.log(err);} 

								// Print to user interface
								vscode.window.showInformationMessage(`stdout: `.concat(stdout));
								vscode.window.showWarningMessage(`stderr: `.concat(stderr));

								// Print to local log
								fs.writeFile(currentFolder
									.concat(`${scriptsSepSym}vscode_stdout.log`)
									,stdout,()=>{}
								);
								fs.writeFile(currentFolder
									.concat(`${scriptsSepSym}vscode_stderr.log`)
									,stderr,()=>{}
								);
								
							});					
						}
					});
					inboxVec.push(newBox);
				}
			inboxVec[inboxVec.length-1].show();
			// If the command has no additional arguments
			}else{
				let command = cp.exec(cmd,(err,stdout,stderr)=>{

					if (err) {console.log(err);} 

					// Print to user interface
					vscode.window.showInformationMessage(`stdout: `.concat(stdout));
					vscode.window.showWarningMessage(`stderr: `.concat(stderr));

					// Print to local log
					fs.writeFile(argsVector[0].concat("/vscode_stdout.log"),stdout,()=>{});
					fs.writeFile(argsVector[0].concat("/vscode_stderr.log"),stderr,()=>{});
					
				});
			}
		});
		context.subscriptions.push(disposable);
	});

	//----------------------------------------------------------------------------------------------

}

// this method is called when your extension is deactivated
export function deactivate() {}
