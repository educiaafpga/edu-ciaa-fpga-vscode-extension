`include "FFD.v"

module test;

   /* GENERAR SEÑALES DE INTERCONEXION */

   //Entrada del FF
   reg in;

   //Salida del FF
   wire q;

   //Reset
   reg rst;

   //Linea de clock
   reg clk = 1'b0;

   /* INSTANCIAR Y CONECTAR DUT */

   FFD ff1 (in,clk,rst,q);

   /* GENERAR ESTIMULOS PERIODICOS */

   //Alternar clock cada 5 unidades de tiempo
   always #5 clk = !clk;

   /* GENERAR ESTIMULOS APERIODICOS */

   initial begin
      $dumpfile("test.vcd"); //Guardar las formas de onda en test.vcd
      $dumpvars(0,test); //Guardar las variables del módulo test

      //Estado inicial
      #0 rst = 1'b1; //Resetear todo
      #0 in = 1'b0; //Entrada en cero
      #1 rst = 1'b0; //Apagar reset

      //Variar la entrada para ver el resultado
      #20 in <= !in;
      
      //Terminar ejecucion 10 ticks despues del ultimo estímulo
      #10 $finish;
   end

endmodule
