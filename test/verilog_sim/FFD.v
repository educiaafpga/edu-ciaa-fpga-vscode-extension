module FFD (

    input wire d,
    input wire clk,
    input wire rst,

    output reg q
);

//Asignación secuencial
always @(posedge clk or posedge rst) begin
	if(rst==1'b1)begin
		q <= 1'b0;
	end else begin
		q <= d;
	end
end

endmodule //FFD
