-------------------------------------------------------
--! @file rsp.vhd
--! @brief Registro Serie-Paralelo
--! @authors Ramiro A. Ghignone
--! @date Jun 10 2021 
--!
--! Este archivo define un Registro Serie-Paralelo
--! formado por una cadena de flip-flops tipo D
--!
--! @vhdlflow REG_SP
------------------------------------------------------

--! Librerias estandar IEEE
library IEEE;

--! Librerias estandar IEEE 1164 para std_logic
use IEEE.std_logic_1164.all; 

--! @brief Entidad de registro serie-paralelo
entity REG_SP is
    generic (
        --! @var N_BITS
        --! @brief Numero de bits del registro
        N_BITS :   integer := 1
    );
    port (
        --! @var input
        --! @brief Entrada de datos
        input  :   in  std_logic;
        --! @var clk
        --! @brief Entrada de clock
        clk    :   in  std_logic;
        --! @var rst
        --! @brief Entrada de reset asincronico
        rst    :   in  std_logic;
        --! @var output
        --! @brief Salida de datos
        output :   out std_logic_vector(N_BITS-1 downto 0)
    );
end entity REG_SP;

--! @brief Definicion de la arquitectura del Registro
architecture REG_SP_ARQ of REG_SP is

-------------------------------------------------------
--! @brief Definicion del componente Flip Flop tipo D
--! @param[in] clk Entrada de clock
--! @param[in] d Entrada de datos
--! @param[in] rst Entrada asincrona de reset
--! @param[out] q Salida
-------------------------------------------------------
component FF_D is
    port (
        clk : in  std_logic;
        d   : in  std_logic;
        rst : in  std_logic;
        q   : out std_logic
    );
end component;    

--Interconexiones auxiliares
signal wires : std_logic_vector(N_BITS downto 0);

begin

    wires(0)<=input;

    --Instanciar e interconectar N_BITS Flip Flop tipo D
    regsp_gen : for i in 0 to N_BITS-1 generate
        ffd : FF_D port map (clk=>clk,rst=>rst,d=>wires(i),q=>wires(i+1));
    end generate regsp_gen;

    --Conectar la salida de la entidad (se utiliza 'downto' para asignar una parte del vector):
    output <= wires(N_BITS downto 1);

end architecture REG_SP_ARQ ;
