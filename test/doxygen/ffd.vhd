-------------------------------------------------------
--! @file ffd.vhd
--! @brief Flip Flop tipo D
--! @authors Ramiro A. Ghignone
--! @date Jun 10 2021 
--!
--! Este archivo define un Flip Flop tipo D con
--! entrada de clock y reset asincronico
--!
--! @vhdlflow FF_D
------------------------------------------------------

--! Librerias estandar IEEE
library IEEE;

--! Librerias estandar IEEE 1164 para std_logic
use IEEE.std_logic_1164.all;

--! @brief Entidad de registro serie-paralelo
entity FF_D is
    port (
        --! @var clk
        --! @brief Entrada de clock
        clk : in  std_logic;
        --! @var d
        --! @brief Entrada de datos
        d   : in  std_logic;
        --! @var rst
        --! @brief Entrada de reset asincronico
        rst : in  std_logic;
        --! @var q
        --! @brief Salida de dato
        q   : out std_logic
    );
end entity FF_D;

--! @brief Definicion de la arquitectura del Flip Flop
architecture FF_D_ARQ of FF_D is
begin

    process (clk,rst) begin
        if(rst='1')then
            q <= '0';
        elsif(rising_edge(clk))then
            q <= d;
        end if;
    end process;

end architecture FF_D_ARQ; 
