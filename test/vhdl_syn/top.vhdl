library IEEE;
use IEEE.std_logic_1164.all;  

entity top is
    port(
        in_p : in  std_logic_vector(3 downto 0);
        out_l: out std_logic_vector(3 downto 0)
    );
end entity top;

architecture arq of top is

signal in_n : std_logic_vector(3 downto 0);

begin

    --Negar las entradas de los pulsadores
    in_n <= not in_p;

    --Asignar salidas segun funciones logicas
    out_l(0) <= in_n(0) and in_n(1) and in_n(2) and in_n(3);
    out_l(1) <= in_n(0) or  in_n(1) or  in_n(2) or  in_n(3);
    out_l(2) <= in_n(0) xor in_n(1) xor in_n(2) xor in_n(3);
    out_l(3) <= in_n(0);

end architecture arq;
    
