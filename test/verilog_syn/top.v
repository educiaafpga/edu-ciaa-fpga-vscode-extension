module  top (
    input wire[3:0] in_p,
    output wire [3:0] out_l
);

    //Elemento auxiliar
    wire [3:0] in_n;

    //Negar las entradas de los pulsadores
    assign in_n = ~in_p;

    //Asignar salidas segun funciones logicas
    assign out_l[0] = in_n[0] & in_n[1] & in_n[2] & in_n[3];
    assign out_l[1] = in_n[0] | in_n[1] | in_n[2] | in_n[3];
    assign out_l[2] = in_n[0] ^ in_n[1] ^ in_n[2] ^ in_n[3];
    assign out_l[3] = in_n[0];

endmodule