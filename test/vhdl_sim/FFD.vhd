-- Flip Flop tipo D
-- Ramiro A. Ghignone - 2020

library IEEE;
use IEEE.std_logic_1164.all;  

entity FF_D is
    port (
        clk : in  std_logic;
        d   : in  std_logic;
        rst : in  std_logic;
        q   : out std_logic
    );
end entity FF_D;

architecture FF_D_ARQ of FF_D is
begin

    process (clk,rst) begin
        if(rst='1')then
            q <= '0';
        elsif(rising_edge(clk))then
            q <= d;
        end if;
    end process;

end architecture FF_D_ARQ; 
