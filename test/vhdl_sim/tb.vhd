-- Testbench para un Flip Flop tipo D, empleando un package propio
-- Ramiro A. Ghignone - 2020

library ieee;                 --Estándar IEEE
use ieee.std_logic_1164.all;  --Biblioteca std_logic_1164
use ieee.numeric_std.all;     --Biblioteca numeric_std

-- Usar package propio
library work;
use work.TestbenchUtils.all;

--Entidad del Testbench (vacío)
entity tb is
end entity tb;

--Arquitectura del Testbench
architecture tb_arq of tb is

--DECLARAR ESTIMULOS

--Entradas al FF
signal d   : std_logic;
signal q   : std_logic;
signal rst : std_logic;
signal clk : std_logic;

--Lista de estimulos
signal stimulus : std_logic_vector(31 downto 0) := x"A0B0C0D0";

--Variable auxiliar para detener la simulación
signal stop_sig : boolean;

--DEFINIR CONSTANTES
constant PERIODO : time := 100 ns;

--DECLARAR DUT
component FF_D is
    port (
        clk : in  std_logic;
        d  : in  std_logic;
        rst   : in  std_logic;
        q     : out std_logic
    );
end component;

begin

  --INSTANCIAR E INTERCONECTAR DUT
  dut : FF_D port map(clk=>clk,d=>d,rst=>rst,q=>q);

  --ESTIMULOS

  --Proceso de reset inicial > usamos las herramientas de TestbenchUtils
  process begin
    ResetGen(0 ns,2*PERIODO,rst);
  end process;

  --Proceso de generación de clock > usamos las herramientas de TestbenchUtils
  process begin
    ClockGen(PERIODO,stop_sig,clk);
  end process;

  --Estimulos del FF_D > usamos las herramientas de TestbenchUtils
  process begin
    stop_sig <= False;

    --Primero, estimulos desde un std_logic_vector
    StimGen(stimulus,PERIODO/3,PERIODO,d);

    --Luego, desde un archivo
    StimFileGen("Stimulus.txt",PERIODO,PERIODO,d);

    stop_sig <= True;
    wait;
  end process;

end architecture tb_arq;
