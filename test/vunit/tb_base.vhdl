--################################################################################
--#
--# tb_base.vhdl
--# This file contains a basic unit test that passes with VUnit
--# Ramiro Adrian Ghignone
--# 2021
--#
--##################################################################################

-- VUnit VHDL libraries
library vunit_lib;
context vunit_lib.vunit_context;

-- Testbench entity
entity tb_base is
  -- Add to all testbenches to be used with VUnit
  generic (runner_cfg : string);
end entity;

architecture tb of tb_base is
begin
  main : process
  begin

    --Begin test
    test_runner_setup(runner, runner_cfg);

    --Nothing goes wrong here
    report "Hello world!";

    --Finish testbench
    test_runner_cleanup(runner);
  end process;
end architecture;
