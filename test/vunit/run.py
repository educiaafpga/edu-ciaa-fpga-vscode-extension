#################################################################################
#
# VUnit run.py file
# This is the main entry point to the unit testing environment
# Ramiro Adrian Ghignone
# 2021
#
##################################################################################

# Import VUnit libraries
from vunit import VUnit

# Create a new VUnit context
vu = VUnit.from_argv()
vu.add_vhdl_builtins()

# Create test lib
lib = vu.add_library("lib")

# Add all the files finished with ".vhdl" to the workspace
# The tool will consider them as testbenches 
# if their names start with "tb_" or finish with "_tb"
lib.add_source_files("*.vhdl")

# Run tests
vu.main()
