--################################################################################
--#
--# tb_base.vhdl
--# This file contains a basic unit test that raises a warning with VUnit
--# Ramiro Adrian Ghignone
--# 2021
--#
--##################################################################################

-- VUnit VHDL libraries
library vunit_lib;
context vunit_lib.vunit_context;

-- Testbench entity
entity tb_warn is
  -- Add to all testbenches to be used with VUnit
  generic (runner_cfg : string);
end entity;

architecture tb of tb_warn is
begin
  main : process
  begin

    --Begin test
    test_runner_setup(runner, runner_cfg);

    --Force a false assertion to report a warning
    assert false report "Warning !!" severity warning;

    --Finish testbench
    test_runner_cleanup(runner);
  end process;
end architecture;
