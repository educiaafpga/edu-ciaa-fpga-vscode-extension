#!/bin/bash
docker run --privileged \
    -v "$1":/home/ghdl/src \
    --rm educiaafpga/x64 /bin/sh \
    -c "cd home/ghdl/src && chmod +x $2 && sh $2"