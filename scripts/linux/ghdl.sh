#!/bin/bash
docker run \
    -v $1:/home/ghdl/src \
    --rm educiaafpga/x64 /bin/sh \
    -c "cd home/ghdl/src ; ghdl -i -fsynopsys -frelaxed --std=08 *.vhdl ;\
        ghdl -i -fsynopsys -frelaxed --std=08 *.vhd ;\
        ghdl -m -fsynopsys -frelaxed --std=08 $2 &&\
        ghdl -r -fsynopsys -frelaxed --std=08 $2 --wave=testbench.ghw ;\
        rm *.cf ; rm *.o" ; gtkwave $1/testbench.ghw