#!/bin/bash
docker run \
    -v $1:/home/doxy/src \
    --rm educiaafpga/x64 /bin/sh \
    -c "cd /home/doxy/src && doxygen -g"