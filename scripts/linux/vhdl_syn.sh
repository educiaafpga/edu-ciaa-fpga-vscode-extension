#!/bin/bash
docker run \
    -v $1:/home/ghdl/syn \
    --rm educiaafpga/x64 /bin/sh \
    -c  "cd home/ghdl/syn ;\
         ghdl -i *.vhdl ;\
         ghdl -i *.vhd ;\
         ghdl -a *.vhdl ;\
         ghdl -a *.vhd ;\
         yosys -m ghdl -p 'ghdl $2\
         ; synth_ice40 -json top.json' " &&\
docker run --privileged \
    -v $1:/home/synth/src \
    --rm educiaafpga/x64 /bin/sh \
    -c  "cd .. &&\
         cd home/synth/src &&\
         nextpnr-ice40 --hx4k --package tq144 --pcf $3 --json top.json --asc top.asc &&\
         icepack top.asc top.bin"