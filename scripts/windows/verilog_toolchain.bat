usbipd wsl attach -i 0403:6010
docker run --privileged --device="/dev/ttyUSB*" -v %1:/home/iVerilog/src --rm educiaafpga/x64 /bin/sh -c " cd .. && cd home/iVerilog/src && yosys -p 'synth_ice40 -json top.json' %2 && nextpnr-ice40 --hx4k --package tq144 --pcf %3 --json top.json --asc top.asc && icepack top.asc top.bin && iceprog top.bin"
rm *.json
rm *.asc
rm *.bin
usbipd wsl detach -i 0403:6010
