[[_TOC_]]

# EDU-CIAA-FPGA for VSCode

![main-pipeline](https://gitlab.com/educiaafpga/edu-ciaa-fpga-vscode-extension/badges/main/pipeline.svg)

This extension provides commands for simulation and synthesis of digital designs in either VHDL or Verilog. It also allows to configure the EDU-CIAA-FPGA board.

## Supported Commands

- VHDL simulation through [GHDL](http://ghdl.free.fr/)
- Verilog simulation through [Icarus Verilog](http://iverilog.icarus.com/)
- Unit Testing through [VUnit](https://vunit.github.io/)
- Synthesis and configuration of the EDU-CIAA-FPGA board using [Yosys](https://github.com/YosysHQ/yosys) + [NextPNR](https://github.com/YosysHQ/nextpnr) + [iCEstorm](http://www.clifford.at/icestorm/)
- Documentation through [Doxygen](https://www.doxygen.nl/index.html)
- Ability to run [Python](https://www.python.org/) or Bash scripts within the context of the Docker container.

## Requirements

You will need [Docker](https://www.docker.com/) installed with the official **educiaafpga** image, which you can find [here](https://hub.docker.com/r/educiaafpga/x64) and download using the following command:
```bash
docker pull educiaafpga/x64
```

## Recomendations

We recommend to install the following VSCode extensions:
 - [Verilog-HDL](https://marketplace.visualstudio.com/items?itemName=mshr-h.VerilogHDL)
 - [Awesome VHDL](https://marketplace.visualstudio.com/items?itemName=puorc.awesome-vhdl)

It is also important to have [GTKWave](http://gtkwave.sourceforge.net/) to visualize resulting waveforms.

## How to...

### ...install :

This extension is not yet available in the marketplace, so please download the latest build in VSIX format from [here](https://gitlab.com/educiaafpga/edu-ciaa-fpga-vscode-extension/-/jobs/artifacts/main/raw/educiaafpga.vsix?job=build-extension) and install using the **Extensions: Install from VSIX** command in the Command Palette (**Ctrl+Shift+P**) or the option with the same name in the Extensions sidebar.

### ...use :

**Open the project folder** which contains your source code and, in the Commands Palette (Ctrl+Shift+P), search for the EDU-CIAA-FPGA commands.

### ...test :

There is a **test** folder on the extension directory which contains simulable and synthesizable VHDL and Verilog designs. Use them to test the extension:
 - **vhdl_sim** : Contains a VHDL design and its testbench, to test VHDL simulation.
 - **vhdl_syn** : Contains a VHDL design and its PCF file, to test VHDL synthesis.
 - **verilog_sim** : Contains a Verilog design and its testbench, to test Verilog simulation.
 - **verilog_syn** : Contains a Verilog design and its PCF file, to test Verilog synthesis.
 - **python** : Contains a Python script.
 - **vunit** : Contains a VHDL design and some unit tests, to use the VUnit framework.
 - **doxygen** : Contains VHDL code documented to test the Doxygen features.

### ...report issues :
Feel free to open an Issue on [our Gitlab repository !](https://gitlab.com/educiaafpga/herramientas) or email us at _edufpga@gmail.com_.

## Known Issues
`None`

## Release Notes

### 0.0.1 (10/04/2021)
- Initial version

### 0.0.2 (18/06/2021)
- Added Doxygen tools and examples

### 0.0.3 (06/07/2021)
- Added Windows compatibility

### 1.0.0 (02/06/2023)
- Updated for public release

### 1.0.1 (03/08/2023)
- Fixed arguments order
